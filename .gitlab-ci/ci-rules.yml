# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2018-2023 the Mesa contributors
# SPDX-FileCopyrightText: 2018-2023 Collabora, Ltd. and the Monado contributors

# YAML anchors for rule conditions
# --------------------------------
.rules-anchors:
  rules:
    # Post-merge pipeline
    - if: &is-post-merge '$CI_PROJECT_NAMESPACE == "monado" && $CI_COMMIT_BRANCH'
      when: on_success
    # Pre-merge pipeline
    - if: &is-pre-merge '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: on_success


# When to automatically run the CI for build jobs
.build-rules:
  rules:
    # If any files affecting the pipeline are changed, build/test jobs run
    # automatically once all dependency jobs have passed
    - changes: &all_paths
      # build sys
      - CMakeLists.txt
      - "*.cmake"
      - "**/*.gradle"
      - "cmake/**/*"
      - "gradle/**/*"
      # GitLab CI
      - ".gitlab-ci.yml"
      - ".gitlab-ci/**/*"
      # Source code
      - "scripts/**/*"
      - "src/**/*"
      when: on_success
    # Otherwise, build/test jobs won't run because no rule matched.


.container-rules:
  rules:
    # Run pipeline by default in the main project if any CI pipeline
    # configuration files were changed, to ensure docker images are up to date
    - if: *is-post-merge
      changes:
      - ".gitlab-ci.yml"
      - ".gitlab-ci/**/*"
      when: on_success
    # Run pipeline by default if it is for a merge request, and any files
    # affecting the pipeline were changed
    - if: *is-pre-merge
      changes:
        *all_paths
      when: on_success
    # Allow triggering jobs manually in other cases if any files affecting the
    # pipeline were changed: Disabled for now because it causes the pipeline on
    # main to be blocked because it gets stuck in the container stage.
    #- changes:
    #    *all_paths
    #  when: manual
    # Otherwise, container jobs won't run because no rule matched.
